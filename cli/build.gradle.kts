import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    java
    kotlin("jvm") version "1.3.40"
    application
}

group = "cc.t0ast.cash"
version = "1.0-SNAPSHOT"

application.applicationName = "cash"
application.mainClassName = "cc.t0ast.cash.cli.MainKt"

repositories {
    mavenCentral()
}

dependencies {
    compile("com.github.ajalt:clikt:2.0.0")
    compile("com.google.code.gson:gson:2.8.5")
    compile(project(":core"))
    compile(kotlin("stdlib-jdk8"))
    testCompile("junit", "junit", "4.12")
    testCompile(project(path = ":testshot", configuration = "default"))
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}
tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}
