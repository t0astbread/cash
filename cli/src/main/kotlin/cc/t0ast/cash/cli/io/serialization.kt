package cc.t0ast.cash.cli.io

import cc.t0ast.cash.models.Account
import cc.t0ast.cash.models.Sink
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.File
import java.lang.reflect.Type

private const val ACCOUNT_FILE = "account.json"
private const val SINKS_FILE = "sinks.json"

private val SINKS_TYPE = object : TypeToken<List<Sink>>() {}.type
private val GSON = Gson()


private fun open(path: String) = File(System.getProperty("user.dir"), path)

private inline fun <reified T> File.readJSON(type: Type? = null): T {
    return GSON.fromJson(readText(), type ?: T::class.java)
}

private fun File.writeJSON(obj: Any) {
    writeText(GSON.toJson(obj))
}


fun deserializeAccount() = open(ACCOUNT_FILE).readJSON<Account>()
fun serializeAccount(account: Account) = open(ACCOUNT_FILE).writeJSON(account)

fun deserializeSinks() = open(SINKS_FILE).readJSON<List<Sink>>(SINKS_TYPE)
fun serializeSinks(sinks: List<Sink>) = open(SINKS_FILE).writeJSON(sinks)
