package cc.t0ast.cash.cli.commands

import cc.t0ast.cash.cli.TEST_RESOURCES
import cc.t0ast.testshot.snapshotTest
import cc.t0ast.testshot.withWorkingDir
import org.junit.Test
import java.io.File

class StatusTest {
    @Test
    fun snapshotTest() {
        val testDir = File(TEST_RESOURCES, "status")
        snapshotTest(testDir) {
            withWorkingDir(testDir.absolutePath) {
                Status().main(listOf())
            }
        }
    }
}