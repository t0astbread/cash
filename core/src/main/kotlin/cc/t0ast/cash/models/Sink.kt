package cc.t0ast.cash.models

data class Sink(val name: String)
