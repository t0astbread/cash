package cc.t0ast.cash.cli.commands

import cc.t0ast.cash.cli.NO_SUCH_SINK
import cc.t0ast.cash.cli.io.deserializeSinks
import cc.t0ast.cash.cli.io.serializeSinks
import cc.t0ast.cash.cli.withErrorHandling
import cc.t0ast.cash.models.Sink
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.subcommands
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.options.option

class Sinks: CliktCommand(invokeWithoutSubcommand = true) {
    init {
        subcommands(
                AddSink(),
                RemoveSink(),
                EditSink()
        )
    }

    override fun run() = withErrorHandling {
        if(context.invokedSubcommand != null)
            return@withErrorHandling

        val sinks = deserializeSinks()

        sinks.forEach {
            println(it.name)
        }
    }
}

class AddSink: CliktCommand(name = "add") {
    val name by argument()

    override fun run() = withErrorHandling {
        val sinks = deserializeSinks().toMutableList()
        sinks.add(Sink(name))
        serializeSinks(sinks)

        context.parent?.command?.main(listOf())
    }
}

class RemoveSink: CliktCommand(name = "remove") {
    val name by argument()

    override fun run() = withErrorHandling {
        val sinks = deserializeSinks().toMutableList()
        sinks.removeIf { it.name == name }
        serializeSinks(sinks)

        context.parent?.command?.main(listOf())
    }
}

class EditSink: CliktCommand(name = "edit") {
    val name by argument()
    val newName by option("--name")

    override fun run() = withErrorHandling {
        val sinks = deserializeSinks().toMutableList()

        val indexToReplace = sinks.indexOfFirst { it.name == name }
        if(indexToReplace == -1) {
            println(NO_SUCH_SINK)
            return@withErrorHandling
        }

        val sinkToEdit = sinks[indexToReplace]
        val newSink = Sink(newName ?: sinkToEdit.name)
        sinks[indexToReplace] = newSink

        serializeSinks(sinks)

        context.parent?.command?.main(listOf())
    }
}
