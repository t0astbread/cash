package cc.t0ast.cash.cli

import cc.t0ast.cash.cli.commands.Cash
import cc.t0ast.cash.cli.commands.Init
import cc.t0ast.cash.cli.commands.Sinks
import cc.t0ast.cash.cli.commands.Status
import com.github.ajalt.clikt.core.subcommands

fun main(args: Array<String>) = Cash().main(args)
