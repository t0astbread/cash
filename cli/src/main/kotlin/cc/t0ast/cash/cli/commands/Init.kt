package cc.t0ast.cash.cli.commands

import cc.t0ast.cash.cli.io.serializeAccount
import cc.t0ast.cash.cli.io.serializeSinks
import cc.t0ast.cash.models.Account
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.types.float

class Init: CliktCommand() {
    private val initialBalance by argument().float()

    override fun run() {
        val newAccount = Account(this.initialBalance)
        serializeAccount(newAccount)

        serializeSinks(listOf())
    }
}