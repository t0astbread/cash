package cc.t0ast.cash.cli

import java.io.FileNotFoundException


val NOT_A_CASH_REGISTER = """
    The current directory is not a valid cash register
    
    Initialize a new cash register with `cash init`
""".trimIndent()

val NO_SUCH_SINK = """
    No sink matches the given selector
""".trimIndent()


fun withErrorHandling(block: () -> Unit) {
    try {
        block()
    }
    catch(exc: FileNotFoundException) {
        println(NOT_A_CASH_REGISTER)
    }
}
