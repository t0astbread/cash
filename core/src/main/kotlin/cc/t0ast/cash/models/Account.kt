package cc.t0ast.cash.models

import java.time.LocalDateTime

data class Account(
        val initialBalance: Float,
        val expenses: List<Expense> = listOf()
) {
    val currentBalance: Float
        get() {
            val now = LocalDateTime.now()
            return this.initialBalance - this.expenses
                    .takeWhile { it.timestamp <= now }
                    .sumByDouble { it.amount.toDouble() }
                    .toFloat()
        }
}
