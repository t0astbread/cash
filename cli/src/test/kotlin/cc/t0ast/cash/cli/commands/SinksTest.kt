package cc.t0ast.cash.cli.commands

import cc.t0ast.cash.cli.TEST_RESOURCES
import cc.t0ast.testshot.snapshotTest
import cc.t0ast.testshot.withWorkingDir
import org.junit.Test
import java.io.File

class SinksTest {
    private val SINKS_TEST_ROOT_FOLDER = File(TEST_RESOURCES, "sinks")
    private val SINKS_FILE = "sinks.json"

    @Test
    fun snapshotTestList() {
        val testFolder = File(SINKS_TEST_ROOT_FOLDER, "list")
        copyFiles(testFolder)

        snapshotTest(testFolder, SINKS_FILE) {
            withWorkingDir(testFolder.absolutePath) {
                Sinks().main(listOf())
            }
        }
    }

    @Test
    fun snapshotTestAdd() {
        val testFolder = File(SINKS_TEST_ROOT_FOLDER, "add")
        copyFiles(testFolder)

        snapshotTest(testFolder, SINKS_FILE) {
            withWorkingDir(testFolder.absolutePath) {
                Sinks().main(listOf("add", "New Sink"))
            }
        }
    }

    @Test
    fun snapshotTestEdit() {
        val testFolder = File(SINKS_TEST_ROOT_FOLDER, "edit")
        copyFiles(testFolder)

        snapshotTest(testFolder, SINKS_FILE) {
            withWorkingDir(testFolder.absolutePath) {
                Sinks().main(listOf("edit", "Good Friend #1", "--name", "Friend #1"))
            }
        }
    }

    @Test
    fun snapshotTestRemove() {
        val testFolder = File(SINKS_TEST_ROOT_FOLDER, "remove")
        copyFiles(testFolder)

        snapshotTest(testFolder, SINKS_FILE) {
            withWorkingDir(testFolder.absolutePath) {
                Sinks().main(listOf("remove", "Good Friend #1"))
            }
        }
    }

    private fun copyFiles(testFolder: File) {
        val sinksFile = File(SINKS_TEST_ROOT_FOLDER, SINKS_FILE)
        val targetSinksFile = File(testFolder, SINKS_FILE)
        targetSinksFile.parentFile.mkdirs()
        sinksFile.copyTo(targetSinksFile, overwrite = true)
    }
}
