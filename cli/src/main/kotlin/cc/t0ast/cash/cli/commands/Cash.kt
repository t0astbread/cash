package cc.t0ast.cash.cli.commands

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.subcommands

class Cash: CliktCommand() {
    init {
        subcommands(
                Init(),
                Status(),
                Sinks()
        )
    }

    override fun run() {
    }
}