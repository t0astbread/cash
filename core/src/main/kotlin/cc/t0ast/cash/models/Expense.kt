package cc.t0ast.cash.models

import java.time.LocalDateTime

data class Expense(
        val amount: Float,
        val sink: Sink,
        val description: String,
        val timestamp: LocalDateTime
)
