package cc.t0ast.cash.cli.commands

import cc.t0ast.cash.cli.io.deserializeAccount
import com.github.ajalt.clikt.core.CliktCommand

class Status: CliktCommand() {
    override fun run() {
        val account = deserializeAccount()
        println("Current balance: ${account.currentBalance}")
    }
}
